from datetime import datetime

users = [
    {'name':'paolo', 'email':'paolo@email.com', 'password': '123'},
    {'name':'filippo', 'email':'filippo@email.com', 'password': 'abc'},
    {'name':'simone', 'email':'simone@email.com', 'password': 'kkk'}
]

def decorate_with_timer(func):
    def wrapper(*args, **kwargs):
        start = datetime.now()
        result = func(*args, **kwargs)
        end = datetime.now()
        print(f'Time spent in the operation: {end-start}')
        return result
    wrapper.__doc__ = func.__doc__
    wrapper.__name__ = func.__name__
    return wrapper


def password_protect(func):
    def wrapper(password, *args, **kwargs):
        user = func(*args, **kwargs)
        if (user['password'] == password):
            return user
        else:
            print('YOU DID NOT HAVE THE PASSWORD!')
            return None
    wrapper.__doc__ = func.__doc__
    wrapper.__name__ = func.__name__
    return wrapper

def msg_decorator(message):
    def inner(func):
        def wrapper(*args, **kwargs):
            print(message)
            return func(*args, **kwargs)
        wrapper.__doc__ = func.__doc__
        wrapper.__name__ = func.__name__
        return wrapper
    return inner

@msg_decorator('Hello World!')
@decorate_with_timer
@password_protect
def find_user_by_name(name):
    return next((user for user in users if user['name'] == name), None)

print(find_user_by_name('123', 'paolo'))