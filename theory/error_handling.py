# ERROR HANDLING
import sys

print('')
try:
    10 * (1/0)
except ZeroDivisionError as err:
    print(err)

print('')
try:
    4 + hello * 3
except NameError as err:
    print(err)

print('')
try:
    2 + 'ciao'
except TypeError as err:
    print(err)

print('')
try:
    10 * (1/0)
except ValueError as err:
    print(err)
except:
    print("Unexpected error:", sys.exc_info()[0])

while True:
    try:
        print('')
        value = input("Please enter a integer: ")
        x = int(value)
        break
    except ValueError as err:
        print('')
        print(err)
        print("Oops! That was no valid integer. Try again...")

print(x)