from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
import os
import sys

app = Flask(__name__)
app.secret_key='123123asdasdasd'
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ['DATABASE_URL']
db = SQLAlchemy(app)

# adding jinja extensions
app.jinja_options['extensions'].append('jinja2.ext.do')

cache = {'counter': 0}

# migrations
from models.product import Product
from models.user import User
from models.role import Role
from models.user_roles import User_roles
migrate = Migrate(app, db)

# bluprints
from blueprints.admin import admin
from blueprints.shop import shop
from blueprints.website import website
app.register_blueprint(admin, url_prefix="/admin")
app.register_blueprint(shop, url_prefix="/shop")
app.register_blueprint(website)