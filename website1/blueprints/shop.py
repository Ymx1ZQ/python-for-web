from flask import request, redirect, url_for, render_template, session, Blueprint
from app import cache
from libraries.auth import is_logged

# repositories
from repositories.article import Article_repository
from repositories.product import Product_repository

shop = Blueprint('shop', __name__)

navbar = {}
@shop.before_request
def set_navbar():
    navbar.clear()
    navbar['about'] = url_for('website.about')
    navbar['shop'] = url_for('shop.homepage')
    navbar['list visits'] = url_for('website.list_visits')
    if is_logged():
        navbar['admin'] = url_for('admin.homepage')
    else:
        navbar['login'] = url_for('login')

# routes
@shop.route('/')
def homepage():
    products = Product_repository.find_all_available()

    return render_template('shop/homepage.htm.j2',
        cache=cache,
        navbar=navbar,
        products=products
    )