from flask import request, redirect, url_for, render_template, session, Blueprint
from app import cache
from libraries.auth import is_logged

# repositories
from repositories.article import Article_repository
from repositories.product import Product_repository

website = Blueprint('website', __name__)

navbar = {}
@website.before_request
def set_navbar():
    navbar.clear()
    navbar['about'] = url_for('website.about')
    navbar['shop'] = url_for('shop.homepage')
    navbar['list visits'] = url_for('website.list_visits')
    if is_logged():
        navbar['admin'] = url_for('admin.homepage')
    else:
        navbar['login'] = url_for('login')

# routes
@website.route('/', methods=['GET', 'POST'])
def homepage():
    cache['counter'] += 1    
    if (request.method == 'POST'):
        cache['counter'] = 0
    
    articles = Article_repository.find_all()

    return render_template('website/homepage.htm.j2',
        cache=cache,
        navbar=navbar,
        articles=articles
    )

@website.route('/list-visits')
def list_visits():
    return render_template('website/list-visits.htm.j2', cache=cache, navbar=navbar)

@website.route('/clear-counter', methods=['POST'])
def clear_counter():
    cache['counter'] = 0
    return redirect(url_for('website.homepage'))

@website.route('/about')
def about():
    return 'Hello, we are students of the amazing Python for Web Course! <a href="/">Go to home section!</a>'

@website.route('/blog/<slug>')
def article(slug):
    article = Article_repository.find_one_by_slug(slug)
    return render_template('website/article.htm.j2',
        cache=cache,
        navbar=navbar,
        article=article
    )