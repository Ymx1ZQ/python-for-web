from flask import request, redirect, render_template, session, url_for
from app import app

# users
users = {
    "paolo": "foo",
    "pippo": "bar"
}

# decorator
def secure(func):
    def wrapper(*args, **kwargs):
        if is_logged():
            return func(*args, **kwargs)
        else:
            return redirect('/auth/login')
    wrapper.__name__ = func.__name__
    return wrapper

# routes
@app.route('/auth/login', methods=['GET', 'POST'])
def login():
    message = ''
    if (request.method == 'POST'):
        username = request.form.get('usr')
        password = request.form.get('psw')
        logged = do_login(username, password)
        if not logged:
            message = 'wrong username or password!'
    if is_logged():
        return redirect(url_for('admin.homepage'))
    return render_template('auth/login.htm.j2', message=message)

@app.route('/auth/logout')
def logout():
    session.pop('username', None)
    return redirect('/')

def do_login(username, password):
    user_password = users.get(username, None)
    if not user_password:
        print(f'no user {username} found!')
        return False

    if user_password == password:
        session['username'] = username
        print(f'user {username} successfully logged in!')
        return True
    else:
        print(f'wrong password inserted for user {username}!')
        return False

def is_logged():
    return bool(session.get('username'))