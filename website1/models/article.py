class Article():
    def __init__(self, slug, category_name, title, description, html_content):
        self.slug = slug
        self.category_name = category_name
        self.title = title
        self.description = description
        self.html_content = html_content