from app import db
from models._base_model import Base_model

class Product(Base_model):

    __tablename__ = 'products'

    name = db.Column('name', db.String(255))
    price = db.Column('price', db.Numeric())
    description = db.Column('description', db.String(255))
    stock = db.Column('stock', db.Integer())
