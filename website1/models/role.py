from app import db
from models._base_model import Base_model

class Role(Base_model):

    __tablename__ = 'roles'
    name = db.Column(db.String(255), nullable=False)