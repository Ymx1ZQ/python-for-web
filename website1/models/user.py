from app import db
from models._base_model import Base_model

class User(Base_model):

    __tablename__ = 'users'

    active = db.Column('active', db.Boolean(), nullable=False, server_default='1')
    email = db.Column(db.String(255), nullable=False, unique=True)
    email_confirmed_at = db.Column(db.DateTime())
    password = db.Column(db.String(255), nullable=False, server_default='')
    
    roles = db.relationship('Role', secondary='user_roles')