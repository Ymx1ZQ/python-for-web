from app import db
from models._base_model import Base_model
from sqlalchemy.dialects.postgresql import UUID

class User_roles(Base_model):

    __tablename__ = 'user_roles'
    user_id = db.Column(UUID(as_uuid=True), db.ForeignKey('users.id', ondelete='CASCADE'), nullable=False)
    role_id = db.Column(UUID(as_uuid=True), db.ForeignKey('roles.id', ondelete='CASCADE'), nullable=False)

  