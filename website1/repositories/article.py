from libraries.contentful import Contentful
from rich_text_renderer import RichTextRenderer
from models.article import Article

class Article_repository():
    @classmethod
    def find_all(cls):
        entries = Contentful.get_all_entries('article')
        articles = []
        for entry in entries:
            article = convert_entry_into_article(entry)
            articles.append(article)
        return articles

    @classmethod
    def find_one_by_slug(cls, slug):
        entry = Contentful.get_entry_by_content_type_and_slug('article', slug)
        if not entry:
            return None
        else:
            return convert_entry_into_article(entry)

def convert_entry_into_article(entry):
    renderer = RichTextRenderer()
    html_content = renderer.render(entry.content)
    category_name = entry.fields()['category'].name
    article = Article(
        slug = entry.slug,
        category_name = category_name,
        title = entry.title,
        description = entry.description,
        html_content = html_content
    )
    return article