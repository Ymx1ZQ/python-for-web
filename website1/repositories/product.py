from models.product import Product
from app import db

class Product_repository():
    @classmethod
    def delete(cls, product):
        
        db.session.delete(product)
        db.session.commit()
        return True

    @classmethod
    def find_all(cls):
        products = db.session.query(Product).all()
        return products

    @classmethod
    def find_all_available(cls):
        products = db.session.query(Product).filter(Product.stock>0).all()
        return products

    @classmethod
    def find_one_by_id(cls,id):
        product = db.session.query(Product).filter(Product.id==id).one()
        return product

    @classmethod
    def insert(cls, to_insert):
        product = Product(
            name = to_insert.get('name', None),
            price = to_insert.get('price', None), 
            description = to_insert.get('description', None), 
            stock = to_insert.get('stock', None)
        )
        
        db.session.add(product)
        db.session.commit()
        return product

    @classmethod
    def update(cls, product, to_update):
        product.name = to_update.get('name', product.name) 
        product.price = to_update.get('price', product.price) 
        product.description = to_update.get('description', product.description) 
        product.stock = to_update.get('stock', product.stock) 
        db.session.commit()
        return product
