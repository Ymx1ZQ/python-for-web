# install miniconda https://docs.conda.io/en/latest/miniconda.html
conda create --name=myenv python=3 # ... we can add all packages I want to install
conda activate myenv # also to switch between environments
conda info
conda deactivate
conda install numpy
conda install --file requirements.txt
