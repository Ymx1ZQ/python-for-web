from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_restful import Resource, Api
import os
import sys
from flask_login import LoginManager

app = Flask(__name__)
app.secret_key='123123asdasdasd'
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ['DATABASE_URL']
db = SQLAlchemy(app)
api = Api(app)

# adding jinja extensions
app.jinja_options['extensions'].append('jinja2.ext.do')

# login manager
login_manager = LoginManager(app)

cache = {'counter': 0}

# migrations
from models.product import Product
from models.user import User
from models.role import Role
from models.user_roles import User_roles
migrate = Migrate(app, db)

# bluprints
from blueprints.admin import admin
from blueprints.website import website
# from blueprints.shop import shop
app.register_blueprint(admin, url_prefix="/admin")
app.register_blueprint(website)
# app.register_blueprint(shop, url_prefix="/shop")

# cli
from cli.user import user_cli
app.cli.add_command(user_cli)

# resources
from resources.product import Product_list, Product
api.add_resource(Product_list, '/api/products')
api.add_resource(Product, '/api/products/<product_id>')
