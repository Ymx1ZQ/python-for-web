from flask import request, redirect, url_for, render_template, Blueprint
from flask_login import login_required, current_user

# repositories
from repositories.article import Article_repository
from repositories.product import Product_repository

admin = Blueprint('admin', __name__)

navbar = {}
@admin.before_request
def set_navbar():
    if not current_user.is_authenticated:
        return redirect('/')

    navbar.clear()
    navbar['logout'] = 'logout'

# routes
@admin.route('/')
def homepage():
    products = Product_repository.find_all()
    return render_template('admin/homepage.htm.j2',
        navbar=navbar,
        products=products
    )

@admin.route('/product-edit/<string:product_id>', methods=['GET', 'POST'])
# @login_required
def product_edit(product_id):
    product = Product_repository.find_one_by_id(product_id)
    if request.method == 'POST':
        product = Product_repository.update(product, request.form.to_dict())
    navbar['back'] = url_for('admin.homepage')
    return render_template('admin/product_edit.htm.j2',
        navbar=navbar,
        product=product
    )

@admin.route('/product-create', methods=['GET', 'POST'])
def product_create():
    if request.method == 'POST':
        product = Product_repository.insert(request.form.to_dict())
        # return redirect(url_for('admin.product_edit', product_id=product.id))
        return redirect(url_for('admin.homepage'))
    navbar['back'] = url_for('admin.homepage')
    return render_template('admin/product_insert.htm.j2',
        navbar=navbar
    )

@admin.route('/product-delete/<string:product_id>', methods=['POST'])
def product_delete(product_id):
    product = Product_repository.find_one_by_id(product_id)
    Product_repository.delete(product)
    return redirect(url_for('admin.homepage'))