from flask import request, redirect, url_for, render_template, session, Blueprint
from app import cache
from flask_login import current_user
import libraries.auth

# repositories
from repositories.product import Product_repository

website = Blueprint('website', __name__)

navbar = {}
@website.before_request
def set_navbar():
    navbar.clear()
    # if current_user.is_authenticated:
    #     navbar['admin'] = '/test' #url_for('admin.homepage')
    # else:
    if not current_user.is_authenticated:
        navbar['signup'] = '/auth/signup'
        navbar['login'] = '/auth/login'
    else:
        navbar['account'] = '/auth/edit_account'
        navbar['admin'] = '/admin'
        navbar['logout'] = '/auth/logout'

# routes
@website.route('/', methods=['GET', 'POST'])
def homepage():
    return render_template('website/homepage.htm.j2',
        navbar=navbar
    )