from flask.cli import AppGroup
import click
from repositories.user import User_repository
from repositories.role import Role_repository
from datetime import datetime
import secrets

user_cli = AppGroup('user')

@user_cli.command('initialize-roles')
def initialize_roles():
    roles_list = ['user', 'admin']
    for name in roles_list:
        if (Role_repository.find_one_by_name(name)):
            print(f'role {name} already exists')
        else:
            role = Role_repository.insert({
                'name': name
            })
            print(f'role {role.name} was created!')

@user_cli.command('create-admin-user')
@click.pass_context
def create_admin_user(context):
    password = secrets.token_urlsafe(8)
    initialize_roles.invoke(context)
    admin_role = Role_repository.find_one_by_name('admin')
    user_role = Role_repository.find_one_by_name('user')

    user_data = {
        'active': True,
        'email': 'admin@admin.com',
        'password': password,
        'user_roles': [admin_role, user_role]
    }
    
    if User_repository.find_one_by_email(user_data['email']):
        print(f'user {user_data["email"]} already exists')
    else:
        user = User_repository.insert(user_data)
        User_repository.update(user,
            {
                "email_confirmed_at": datetime.now(),
                "reset_password_token": None
            }
        )
        print(f'user {user.email} created with password {password}')

@user_cli.command('create-role')
@click.argument('name')
def create_role(name):
    if (Role_repository.find_one_by_name(name)):
        print(f'role {name} already exists')
    else:
        role = Role_repository.insert({
            'name': name
        })
        print(f'role {role.name} was created!')


