import re

def is_email(string):
    return bool(
        re.match(r"[^@]+@[^@]+\.[^@]+", string)
    )