from flask import flash, get_flashed_messages, request, redirect, render_template, session, url_for
from flask_login import current_user, login_user, logout_user
from flask_restful import abort, reqparse
from app import app
from app import login_manager
from repositories.user import User_repository
from helpers.string import is_email
from datetime import datetime
from libraries.sendingblue import Sendingblue
import os

# routes
@app.route('/auth/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect('/')

    message = get_flashed_messages()[0] if get_flashed_messages() else ''
    email = ''
    if (request.method == 'POST'):
        email = request.form.get('email')
        password = request.form.get('password')
    
        valid, message, user = is_login_valid(email, password)
        if valid:
            login_user(user)
            print(f'user {user.id} logged in!')

            return redirect('/')

    return render_template('auth/login.htm.j2',
        message=message, email=email)

@app.route('/auth/logout')
def logout():
    logout_user()
    return redirect('/')

@app.route('/auth/signup', methods=['GET', 'POST'])
def signup():
    message = ''
    email = ''
    if (request.method == 'POST'):
        email = request.form.get('email')
        password = request.form.get('password')
        confirm_password = request.form.get('confirm_password')
    
        valid, message = is_signup_valid(email, password, confirm_password)
        if valid:
            user = do_signup(email, password)
            print(f'user {user.id} created!')

            return render_template('auth/confirm_email.htm.j2',
                message=message, email=email)

    return render_template('auth/signup.htm.j2',
        message=message, email=email)

@app.route('/auth/confirm-email/<token>', methods=['GET', 'POST'])
def confirm_email(token):
    user = User_repository.find_one_by_reset_password_token(token)
    if not user:
        flash('the token provided is not valid! please login')
        return redirect(url_for('login'))
    if user.email_confirmed_at:
        flash('email already confirmed! please login')
        return redirect(url_for('login'))

    User_repository.update(user,
        {
            "email_confirmed_at": datetime.now(),
            "reset_password_token": None
        }
    )
    flash('email confirmed! now you can login with your credentials')
    return redirect(url_for('login'))

@app.route('/auth/reset-password', methods=['GET', 'POST'])
def reset_password():
    return "hello world"


# helpers

def is_login_valid(email, password):
    user = User_repository.find_one_by_email(email)
    if user and user.check_password(password):
        return True, 'you successfully logged in!', user
    else:
        return False, 'email and password combination provided is incorrect.', None

def do_signup(email, password):
    user_data = {
        "email": email,
        "password": password
    }

    user = User_repository.insert(user_data)

    email_response = Sendingblue.send_email(
        subject='password confirmation for python website!',
        html_content=render_template('auth/emails/confirm_email.htm.j2', email=user.email, reset_password_token=user.reset_password_token),
        sender={"email":os.environ['SENDER_EMAIL']},
        to=[{"email": user.email}]
    )

    if not email_response:
        flash('we had a problem with the creation of your account, plese contact our support.')
        return None

    return user

def is_signup_valid(email, password, confirm_password):
    if not is_email(email):
        return False, 'email is not a real email!'

    if (len(password) < 6):
        return False, 'password is too short (at least 6 chars)!'

    if (password != confirm_password):
        return False, 'confirm password does not match!'

    if (User_repository.find_one_by_email(email)):
        return False, 'another user with this email already exists!'

    return True, ''

def do_signup(email, password):
    user_data = {
        "email": email,
        "password": password
    }

    user = User_repository.insert(user_data)

    email_response = Sendingblue.send_email(
        subject='password confirmation for python website!',
        html_content=render_template('auth/emails/confirm_email.htm.j2', email=user.email, reset_password_token=user.reset_password_token),
        sender={"email":os.environ['SENDER_EMAIL']},
        to=[{"email": user.email}]
    )

    if not email_response:
        flash('we had a problem with the creation of your account, plese contact our support.')
        return None

    return user

@login_manager.user_loader
def load_user(email):
    return User_repository.find_one_by_email(email)

def api_token_exists(func):
    def wrapper(*args, **kwargs):

        parser = reqparse.RequestParser()
        parser.add_argument('token', type=str)
        token = parser.parse_args()['token']
        if not token:
            return abort(401, message=f'no auth token provided')
        elif not User_repository.find_one_by_api_token(token):
            return abort(401, message=f'{token} is not a valid auth valid')
        else:
            return func(*args, **kwargs)

    wrapper.__doc__ = func.__doc__
    wrapper.__name__ = func.__name__
    return wrapper
    
# def login_required(role="ANY"):
#     def wrapper(fn):
#         @wraps(fn)
#         def decorated_view(*args, **kwargs):

#             if not current_user.is_authenticated():
#                return current_app.login_manager.unauthorized()
#             urole = current_app.login_manager.reload_user().get_urole()
#             if ( (urole != role) and (role != "ANY")):
#                 return current_app.login_manager.unauthorized()      
#             return fn(*args, **kwargs)
#         return decorated_view
#     return wrapper