import contentful
import logging
import os

class Contentful:
    client = contentful.Client(
        os.environ['CONTENTFUL_SPACE_ID'],
        os.environ['CONTENTFUL_DELIVERY_TOKEN']
    )

    @classmethod
    def get_all_entries(cls, content_type):
        return cls.client.entries({'content_type': content_type})

    @classmethod
    def get_entry_by_content_type_and_slug(cls, content_type, slug):
        entries = cls.client.entries({
            'content_type': content_type,
            'fields.slug': slug
        })
        if entries:
            return entries[0]
        else:
            return None