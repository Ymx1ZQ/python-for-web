import sib_api_v3_sdk
from sib_api_v3_sdk.rest import ApiException
import os

class Sendingblue:
    configuration = sib_api_v3_sdk.Configuration()
    configuration.api_key['api-key'] = os.environ['SENDINGBLUE_API_KEY']

    @classmethod
    def send_email(self, subject:str, html_content:str, sender:dict, to:list, cc:list=None, bcc:list=None, reply_to:dict=None):
        print(subject, html_content, sender, to)

        if not reply_to:
            reply_to = sender
        
        api_instance = sib_api_v3_sdk.TransactionalEmailsApi(sib_api_v3_sdk.ApiClient(self.configuration))
        send_smtp_email = sib_api_v3_sdk.SendSmtpEmail(
            subject=subject,
            html_content=html_content,
            sender=sender,
            to=to,
            bcc=bcc,
            cc=cc,
            reply_to=reply_to
        )

        try:
            api_response = api_instance.send_transac_email(send_smtp_email)
            print(api_response)
            return True
        except ApiException as e:
            print("Exception when calling SMTPApi->send_transac_email: %s\n" % e)
            return False