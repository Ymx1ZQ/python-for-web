from app import db
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.sql import func
import uuid
import datetime
import decimal

class Base_model(db.Model):
    __abstract__ = True
    id = db.Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4, unique=True, nullable=False, index=True)
    created_at = db.Column(db.DateTime(timezone=True), server_default=func.now())
    updated_at = db.Column(db.DateTime(timezone=True), onupdate=func.now())

    def as_dict(self):
        dictionary = {}
        for column in self.__table__.columns:
            if column.name != "__class__":
                column_value = getattr(self, column.name)
                dictionary[column.name] = to_serializable_value(column_value)
        return dictionary

def to_serializable_value(value):
    value_type = type(value)
    if value_type == uuid.UUID:
        return str(value)
    elif value_type == datetime.datetime:
        return value.isoformat()
    elif value_type == decimal.Decimal:
        return float(value)
    else: 
        return value