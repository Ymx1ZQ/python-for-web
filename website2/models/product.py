from app import db
from models._base_model import Base_model

class Product(Base_model):

    __tablename__ = 'products'

    name = db.Column('name', db.String(255))
    price = db.Column('price', db.Numeric())
    description = db.Column('description', db.String(255))
    stock = db.Column('stock', db.Integer())

    # def as_dict(self):
    #     return {
    #         "id": str(self.id),
    #         "created_at": self.created_at.isoformat() if not self.created_at == None else None,
    #         "updated_at": self.updated_at.isoformat() if not self.updated_at == None else None,
    #         "name": self.name,
    #         "price": float(self.price),
    #         "description": self.description,
    #         "stock": self.stock,
    #     }