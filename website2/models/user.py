from app import db
from models._base_model import Base_model
from flask_login import UserMixin
import bcrypt
import secrets

class User(UserMixin, Base_model):

    __table_args__ = {'extend_existing': True}
    __tablename__ = 'users'

    active = db.Column('active', db.Boolean(), nullable=False, server_default='1')
    email = db.Column(db.String(255), nullable=False, unique=True, index=True)
    email_confirmed_at = db.Column(db.DateTime())
    reset_password_token = db.Column(db.String(255), index=True)
    api_token = db.Column(db.String(255), unique=True, index=True)

    password = db.Column(db.String(255), nullable=False, server_default='')
    roles = db.relationship('Role', secondary='user_roles')

    def get_id(self):
        return str(self.email)

    def set_password(self, password):
        print(password)
        self.salt = bcrypt.gensalt()
        hash = bcrypt.hashpw(password.encode('utf8'), self.salt)
        self.password = hash.decode('utf8')

    def check_password(self, password):
        print(password)
        return bcrypt.checkpw(password.encode('utf8'), self.password.encode('utf8'))

    def generate_reset_password_token(self):
        self.reset_password_token = secrets.token_urlsafe(34)

    # def is_active(self):
    #     return True

    # def is_authenticated(self):
    #     return True

    # def is_anonymous(self):
    #     return False