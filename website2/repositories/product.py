from models.product import Product
from app import db

class Product_repository():
    @classmethod
    def delete(cls, product):
        
        db.session.delete(product)
        db.session.commit()
        return True

    @classmethod
    def find_all(cls):
        products = db.session.query(Product).all()
        return products

    @classmethod
    def find_all_available(cls):
        products = db.session.query(Product).filter(Product.stock>0).all()
        return products

    @classmethod
    def find_one_by_id(cls,id):
        product = db.session.query(Product).filter(Product.id==id).first()
        return product

    @classmethod
    def insert(cls, data):
        product = Product(
            name = data.get('name', None),
            price = data.get('price', None), 
            description = data.get('description', None), 
            stock = data.get('stock', None)
        )
        
        db.session.add(product)
        db.session.commit()
        return product

    @classmethod
    def update(cls, product, data):
        product.name = data.get('name', product.name) 
        product.price = data.get('price', product.price) 
        product.description = data.get('description', product.description) 
        product.stock = data.get('stock', product.stock) 
        db.session.commit()
        return product
