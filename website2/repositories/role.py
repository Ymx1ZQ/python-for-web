from models.role import Role
from app import db

class Role_repository():
    @classmethod
    def delete(cls, role):
        db.session.delete(role)
        db.session.commit()
        return True

    @classmethod
    def find_all(cls):
        roles = db.session.query(Role).all()
        return roles

    @classmethod
    def find_one_by_id(cls,id):
        role = db.session.query(Role).filter(Role.id==id).first()
        return role

    @classmethod
    def find_one_by_name(cls,name):
        role = db.session.query(Role).filter(Role.name==name).first()
        return role

    @classmethod
    def insert(cls, role_data):
        # admin_role = Role_repository.find_one_by_name('admin')
        role = Role(
            name = role_data.get('name'),
        )
        db.session.add(role)
        db.session.commit()
        return role

    @classmethod
    def update(cls, role, to_update):
        role.name = to_update.get('name', role.name) 
        db.session.commit()
        return role
