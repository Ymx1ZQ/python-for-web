from models.user import User
from models.role import Role
from app import db

class User_repository():
    @classmethod
    def delete(cls, user):
        db.session.delete(user)
        db.session.commit()
        return True

    @classmethod
    def find_all(cls):
        users = db.session.query(User).all()
        return users

    @classmethod
    def find_one_by_id(cls,id):
        user = db.session.query(User).filter(User.id==id).first()
        return user

    @classmethod
    def find_one_by_email(cls,email):
        user = db.session.query(User).filter(User.email==email).first()
        return user

    @classmethod
    def find_one_by_reset_password_token(cls,token):
        user = db.session.query(User).filter(User.reset_password_token==token).first()
        return user

    @classmethod
    def find_one_by_api_token(cls,token):
        user = db.session.query(User).filter(User.api_token==token).first()
        return user

    @classmethod
    def insert(cls, user_data):
        user = User(
            active = True,
            email = user_data.get('email'),
            roles = user_data.get('user_roles', [])
        )
        user.set_password(user_data.get('password'))
        user.generate_reset_password_token()
        db.session.add(user)
        db.session.commit()
        return user

    @classmethod
    def update(cls, user, to_update):
        user.email_confirmed_at = to_update.get('email_confirmed_at', user.email_confirmed_at) 
        user.reset_password_token = to_update.get('reset_password_token', user.reset_password_token) 
        db.session.commit()
        return user
