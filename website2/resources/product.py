from flask_restful import abort, Resource, reqparse
from repositories.product import Product_repository
from libraries.auth import api_token_exists
import uuid
import json

class Product_list(Resource):
    decorators = [api_token_exists]

    # @api_token_exists
    def get(self):
        products = Product_repository.find_all()
        product_ids = [product.as_dict() for product in products]
        return {
            'products': [
                product_ids
            ]
        }

    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('name', type=str, required=True, help='The name cannot be blank!')
        parser.add_argument('price', type=float, required=True, help='You have to add a float price!')
        parser.add_argument('description', type=str)
        parser.add_argument('stock', type=int, required=True, help='You should add a value to it!')
        args = parser.parse_args()
        
        data = {
            "name": args['name'],
            "price": args['price'],
            "description": args['description'],
            "stock": args['stock']
        }
        product = Product_repository.insert(data)
        return {
            'product': product.as_dict()
        }

class Product(Resource):
    # decorators = [api_token_exists]

    def delete(self, product_id):
        product = get_product(product_id)
        Product_repository.delete(product)
        return None, 204

    def get(self, product_id):
        product = get_product(product_id)
        return {
            'product': product.as_dict()
        }
        
    def put(self, product_id):
        product = get_product(product_id)
        parser = reqparse.RequestParser()
        parser.add_argument('name', type=str)
        parser.add_argument('price', type=float, help='You have to add a float price!')
        parser.add_argument('description', type=str)
        parser.add_argument('stock', type=int, help='You should add a value to it!')
        args = parser.parse_args()
        data = {}
        for key, value in args.items():
            if value:
                data[key] = value
        return Product_repository.update(product, data).as_dict()

def get_product(product_id):
    try:
        uuid_obj = uuid.UUID('{'+product_id+'}')
    except TypeError as error:
        print(error)
        abort(400, message=f'{product_id} is not a valid ID')

    product = Product_repository.find_one_by_id(product_id)
    if product:
        return product
    else:
        abort(404, message=f'product {product_id} doesn\'t exist')